export default {
    // Target: https://go.nuxtjs.dev/config-target
    target: 'static',

    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'Grêmio Vinícius de Moraes',
        titleTemplate: 'GVM - %s',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Site para atualizações do Grêmio Estudantil Vinícius de Moraes da Escola Elisa Tramontina.' },
            { name: 'format-detection', content: 'telephone=no' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
    // https://go.nuxtjs.dev/typescript
        '@nuxt/typescript-build',
        // https://go.nuxtjs.dev/tailwindcss
        '@nuxtjs/tailwindcss',
        // https://google-fonts.nuxtjs.org
        '@nuxtjs/google-fonts',
        // https://marquez.co/docs/nuxt-netlify
        '@aceforth/nuxt-netlify',
        // https://google-analytics.nuxtjs.org
        '@nuxtjs/google-analytics'
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
    // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        // https://go.nuxtjs.dev/pwa
        '@nuxtjs/pwa',
        // https://go.nuxtjs.dev/content
        '@nuxt/content',
        // AdSense (https://github.com/nuxt-community/google-adsense-module)
        ['@nuxtjs/google-adsense', {
            id: 'ca-pub-6679501010817555'
        }],
        // https://sitemap.nuxtjs.org/guide/setup
        '@nuxtjs/sitemap',
        // https://github.com/nuxt-community/robots-module
        '@nuxtjs/robots',
        // https://github.com/Developmint/nuxt-svg-loader
        'nuxt-svg-loader'
    ],

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {},

    // PWA module configuration: https://go.nuxtjs.dev/pwa
    pwa: {
        manifest: {
            lang: 'pt-br'
        }
    },

    // Content module configuration: https://go.nuxtjs.dev/config-content
    content: {},

    sitemap: {
        hostname: 'https://gremiovm.de'
    },

    googleFonts: {
        families: {
            Oxygen: true,
            'Cinzel Decorative': [400, 700]
        },
        display: 'swap',
        download: true
    },

    googleAnalytics: {
        id: 'UA-153567898-1'
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
    }
}
