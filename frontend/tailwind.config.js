module.exports = {
    purge: [],
    darkMode: false, // or 'media' or 'class'
    theme: {
        fontFamily: {
            sans: ['Oxygen', 'DM Sans', 'Roboto', 'Arial'],
            cursive: 'Cinzel Decorative',
            mono: ['DM Mono']
        },
        extend: {
            colors: {
                blue1: '#0a1c40',
                blue2: '#122a5a',
                blue3: '#4178bf',
                blue4: '#7eaed9'
            },
            typography: {
                default: {
                    css: {
                        'code::before': {
                            content: '""'
                        },
                        'code::after': {
                            content: '""'
                        }
                    }
                }
            }
        }
    },
    variants: {
        extend: {}
    },
    plugins: [
        require('@tailwindcss/typography')
    ]
}
